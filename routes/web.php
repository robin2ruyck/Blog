<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/', 'PostController', ['except' => ['show', 'edit', 'update']]);
/*Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/myspace', 'MySpaceController@index')->name('myspace');
Route::get('/myspace/post/add', 'PostController@create')->name('post_add');
//Route::get('/myspace/post/edit', 'PostController@edit')->name('article_edit');

