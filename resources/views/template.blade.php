<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mon joli site</title>
    <link href="{{ asset('https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}" rel="stylesheet">
    <style> textarea { resize: none; } </style>
</head>
<body>
<header class="jumbotron">
    <div class="container">
        <h1 class="page-header"><a  class="btn btn-warning" href="{{ route('home') }}">Accueil</a></h1>
        @yield('header')
    </div>
</header>
<div class="container">
    @yield('contenu')
</div>
</body>
</html>