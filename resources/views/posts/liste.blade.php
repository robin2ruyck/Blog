@extends('template')

@section('header')
@if(Auth::check())
<div class="btn-group pull-right">
    <a  class="btn btn-info" href="{{ route('post_add') }}">Ajouter un article</a>
</div>
@else

<a  class="btn btn-info pull-right" href="{{ route('login') }}">Login</a>
@endif
@endsection

@section('contenu')
@if(isset($info))
<div class="row alert alert-info">{{ $info }}</div>
@endif
{{ $links }}
@foreach($posts as $post)
<article class="row bg-primary">
    <div class="col-md-12">
        <header>
            <h1>{{ $post->titre }}</h1>
        </header>
        <hr>
        <section>
            <p>{{ $post->contenu }}</p>
            <em class="pull-right">
                <span class="glyphicon glyphicon-pencil"></span> {{ $post->user->name }} le {!! $post->created_at->format('d-m-Y') !!}
            </em>
        </section>
    </div>
</article>
<br>
@endforeach
{{ $links }}
@endsection