@extends('template')

@section('contenu')
<br>
<div class="col-sm-offset-3 col-sm-6">
    <div class="panel panel-info">
        <div class="panel-heading">Ajout d'un article</div>
        <div class="panel-body">

            <form method="POST" action="{{ route('post.store') }}">
                @csrf

                <div class="form-group row">
                    <label for="titre" class="col-md-4 col-form-label text-md-right">Titre</label>

                    <div class="col-md-6">
                        <input id="titre" type="text" class="form-control{{ $errors->has('Titre') ? 'has-error' : '' }}" name="titre" value="{{ old('Titre') }}" required autofocus>

                        @if ($errors->has('Titre'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('Titre') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contenu" class="col-md-4 col-form-label text-md-right">Contenu</label>

                    <div class="col-md-6">
                        <textarea id="contenu" type="textarea" class="form-control{{ $errors->has('contenu') ? 'has-error' : '' }}" name="contenu" value="{{ old('contenu') }}" required></textarea>

                        @if ($errors->has('contenu'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contenu') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Enregistrer
                        </button>
                    </div>
                </div>
            </form>





        </div>
    </div>
    <a href="javascript:history.back()" class="btn btn-primary">
        <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
    </a>
</div>
@endsection