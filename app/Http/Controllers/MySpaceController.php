<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MySpaceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('myspace');
    }

    public function add()
    {
        return view('articles.add');
    }
}
