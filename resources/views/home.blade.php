@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($posts as $post)
                    <article class="row bg-primary">
                        <div class="col-md-12">
                            <header>
                                <h1>{{ $post->titre }}</h1>
                            </header>
                            <hr>
                            <section>
                                <p>{{ $post->contenu }}</p>
                                <em class="pull-right">
                                    <span class="glyphicon glyphicon-pencil"></span> {{ $post->user->name }} le {!! $post->created_at->format('d-m-Y') !!}
                                </em>
                            </section>
                        </div>
                    </article>
                    <br>
                    @endforeach
                    <a class="btn btn-primary" href="{{ route('post_add') }}">Ajouter un article</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
